# MIT_Linear_Algebra_mxnet_plotly

My version of MIT_OCW_Linear_Algebra_18_06 ipython notebooks, but written using mxnet and plotly. This is so I can practice mxnet and plotly while learning Linear Algebra.